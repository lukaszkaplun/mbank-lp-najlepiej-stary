$(document).ready(function(){
    $('.arrow-scroll').click(function () {
       
        $('html, body').animate({
            scrollTop: $('#slidebox-'+$(this).attr('id').split('-')[1]).offset().top
        }, 1000);
    });

    $('.arrow-18scroll').click(function () {
        $('html, body').animate({
            scrollTop: $('#slidebox18-'+$(this).attr('id').split('-')[1]).offset().top
        }, 1000);
    });

    $('.faq-question').click(function(){
        if(!$(this).hasClass('active-q')){
            $(this).addClass('active-q');
        } else {
            $(this).removeClass('active-q');
        }

        if(!$('#faqposition_'+$(this).attr('id').split('_')[1]).hasClass('active-p')){
            $('#faqposition_'+$(this).attr('id').split('_')[1]).addClass('active-p');
        } else {
            $('#faqposition_'+$(this).attr('id').split('_')[1]).removeClass('active-p');
        }

        $('#faqanswer_'+$(this).attr('id').split('_')[1]).slideToggle();
    });

    $('.slide-faq').click(function(){
        $('html, body').animate({
            scrollTop: $('#faq-section').offset().top
        }, 1000);
    });

    $('#slidetop').click(function () {
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });

    /*
    $('#13-17-ages').addClass('active-age');
    $('#content-13-17').addClass('active-age-content');
    $('.button-1217').addClass('show-button');

    $('#13-17-ages').click(function(){
        if(!$(this).hasClass('active-age')) {
            $('#18-more-ages').removeClass('active-age');
            $(this).addClass('active-age');
        }
        if(!$('#content-13-17').hasClass('active-age-content')) {
            $('#content-18-more').removeClass('active-age-content');
            $('#content-13-17').addClass('active-age-content');
        }

        $('.only-18').removeClass('show-it');

        $('.button-1217').addClass('show-button');
        $('.button-18more').removeClass('show-button');
    });

    $('#18-more-ages').click(function(){
        if(!$(this).hasClass('active-age')) {
            $('#13-17-ages').removeClass('active-age');
            $(this).addClass('active-age');
        }
        if(!$('#content-18-more').hasClass('active-age-content')) {
            $('#content-13-17').removeClass('active-age-content');
            $('#content-18-more').addClass('active-age-content');
        }

        $('.only-18').addClass('show-it');

        $('.button-1217').removeClass('show-button');
        $('.button-18more').addClass('show-button');
    });
    */
});